with open('dump_sem_id.sql', 'r') as file:
    lines = file.readlines()

new_id = 1
for i in range(len(lines)):
    if lines[i].startswith('INSERT INTO "loan_data" VALUES('):
        lines[i] = lines[i].replace('INSERT INTO "loan_data" VALUES(', f'INSERT INTO "loan_data" VALUES({new_id},')
        new_id += 1

with open('output.sql', 'w') as file:
    file.writelines(lines)