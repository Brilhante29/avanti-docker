from app.config import DATABASE_URL

from sqlalchemy import (Column, Integer, String, Float, Table, create_engine, MetaData, DateTime)
from sqlalchemy.sql import func
from databases import Database
from datetime import datetime as dt
from pytz import timezone as tz

# SQLAlchemy
engine = create_engine(DATABASE_URL)
metadata = MetaData()
database = Database(DATABASE_URL)
loan_data = Table(
    "loan_data",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("person_age", Integer),
    Column("person_income", Integer),
    Column("person_emp_length", Float),
    Column("loan_amnt", Integer),
    Column("loan_int_rate", Float),
    Column("loan_percent_income", Float),
    Column("cb_person_cred_hist_length", Integer),
    Column("person_home_ownership", String(50)),
    Column("loan_intent", String(50)),
    Column("loan_grade", String(2)),
    Column("cb_person_default_on_file", String(1)),
    Column("loan_income_ratio", Float),
    Column("age_cred_hist_interaction", Integer),
    Column("log_person_income", Float),
    Column("created_date", DateTime, default=func.now())
)