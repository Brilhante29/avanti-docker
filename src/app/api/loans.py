import os

import numpy as np
from app.api import crud
from app.api.models import LoanDataDB, LoanDataRequest, PredictResponse, TrainModelResponse
from fastapi import APIRouter, HTTPException, Path, Request
from typing import List, Dict
from joblib import load

router = APIRouter()


@router.post("/", response_model=LoanDataDB, status_code=201)
async def create_loan(payload: LoanDataRequest):
    loan_id = await crud.post(payload)
    response_object = await crud.get(loan_id)
    return response_object


@router.get("/{id}/", response_model=LoanDataDB)
async def read_loan(id: int = Path(..., gt=0)):
    loan = await crud.get(id)
    if not loan:
        raise HTTPException(status_code=404, detail="Loan not found")
    return loan


@router.get("/", response_model=List[LoanDataDB])
async def read_all_loans(request: Request):
    return await crud.get_all()


@router.put("/{id}/", response_model=LoanDataDB)
async def update_loan(payload: LoanDataRequest, id: int = Path(..., gt=0)):
    loan = await crud.get(id)
    if not loan:
        raise HTTPException(status_code=404, detail="Loan not found")
    await crud.put(id, payload)
    response_object = await crud.get(id)
    return response_object


@router.delete("/{id}/", response_model=LoanDataDB)
async def delete_loan(id: int = Path(..., gt=0)):
    loan = await crud.get(id)
    if not loan:
        raise HTTPException(status_code=404, detail="Loan not found")
    await crud.delete(id)
    return loan


@router.get("/example_error")
async def example_error():
    await crud.example_error_db()


@router.post("/train", response_model=TrainModelResponse)
async def train_model(request: Request):
    data = await request.json()
    train_response = await crud.train_model(data)
    return train_response


@router.post("/predict", response_model=PredictResponse)
async def predict(data: LoanDataRequest):
    model_path = os.path.join(os.getcwd(), 'app', 'api', 'best_credit_risk_model.joblib')
    model = load(model_path)
    input_data = [data.dict().values()]
    prediction = model.predict(input_data)[0]
    prediction_prob = model.predict_proba(input_data)[0]
    
    if prediction == 1:
        result = "Approved"
    else:
        result = "Denied"
    
    return await PredictResponse(
        prediction=result,
        probability_of_approval=prediction_prob[1],
        probability_of_denial=prediction_prob[0]
    )