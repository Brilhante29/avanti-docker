from app.api.models import LoanDataRequest, TrainModelResponse
from app.db import loan_data, database
from app.config import logger
from datetime import datetime as dt
from pytz import timezone as tz
import numpy as np
from sklearn.model_selection import train_test_split, GridSearchCV, cross_validate
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from xgboost import XGBClassifier
from joblib import dump
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score
from typing import List, Dict
import pandas as pd

class FeatureEngineering:
    def transform(self, data):
        data['loan_income_ratio'] = data['loan_amnt'] / data['person_income']
        data['age_cred_hist_interaction'] = data['person_age'] * data['cb_person_cred_hist_length']
        data['log_person_income'] = np.log1p(data['person_income'])
        return data


async def post(payload: LoanDataRequest):
    payload_dict = payload.dict()
    transformed_payload = FeatureEngineering().transform(payload_dict)

    query = loan_data.insert().values(
        person_age=payload.person_age,
        person_income=payload.person_income,
        person_emp_length=payload.person_emp_length,
        loan_amnt=payload.loan_amnt,
        loan_int_rate=payload.loan_int_rate,
        loan_percent_income=payload.loan_percent_income,
        cb_person_cred_hist_length=payload.cb_person_cred_hist_length,
        person_home_ownership=payload.person_home_ownership,
        loan_intent=payload.loan_intent,
        loan_grade=payload.loan_grade,
        cb_person_default_on_file=payload.cb_person_default_on_file,
        loan_income_ratio=transformed_payload['loan_income_ratio'],
        age_cred_hist_interaction=transformed_payload['age_cred_hist_interaction'],
        log_person_income=transformed_payload['log_person_income'],
        created_date=dt.now(tz("America/Sao_Paulo")).strftime("%Y-%m-%d %H:%M")
    )
    return await database.execute(query=query)


async def get_all():
    query = loan_data.select()
    return await database.fetch_all(query=query)


async def get(id: int):
    query = loan_data.select().where(id == loan_data.c.id)
    return await database.fetch_one(query=query)


async def put(id: int, payload: LoanDataRequest):
    payload_dict = payload.dict()
    transformed_payload = FeatureEngineering().transform(payload_dict)

    created_date = dt.now(tz("America/Sao_Paulo")).strftime("%Y-%m-%d %H:%M")
    query = (
        loan_data.update().where(id == loan_data.c.id).values(
            person_age=payload.person_age,
            person_income=payload.person_income,
            person_emp_length=payload.person_emp_length,
            loan_amnt=payload.loan_amnt,
            loan_int_rate=payload.loan_int_rate,
            loan_percent_income=payload.loan_percent_income,
            cb_person_cred_hist_length=payload.cb_person_cred_hist_length,
            person_home_ownership=payload.person_home_ownership,
            loan_intent=payload.loan_intent,
            loan_grade=payload.loan_grade,
            cb_person_default_on_file=payload.cb_person_default_on_file,
            loan_income_ratio=transformed_payload['loan_income_ratio'],
            age_cred_hist_interaction=transformed_payload['age_cred_hist_interaction'],
            log_person_income=transformed_payload['log_person_income'],
            created_date=created_date
        )
        .returning(loan_data.c.id)
    )
    return await database.execute(query=query)


async def delete(id: int):
    query = loan_data.delete().where(id == loan_data.c.id)
    return await database.execute(query=query)


def evaluate_model(model, X_test, y_test):
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    auc_roc = roc_auc_score(y_test, y_pred)
    return accuracy, precision, recall, f1, auc_roc


async def train_model(data: List[Dict]):
    df = pd.DataFrame(data)
    df['person_emp_length'].fillna(df['person_emp_length'].median(), inplace=True)
    df['loan_int_rate'].fillna(df['loan_int_rate'].median(), inplace=True)
    df = df[df['person_age'] <= 100]
    df = df[df['person_income'] <= 500000]

    X = df.drop('loan_status', axis=1)
    y = df['loan_status']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

    numeric_features = ['person_age', 'person_income', 'person_emp_length', 'loan_amnt', 'loan_int_rate', 'loan_percent_income', 'cb_person_cred_hist_length', 'loan_income_ratio', 'age_cred_hist_interaction', 'log_person_income']
    categorical_features = ['person_home_ownership', 'loan_intent', 'loan_grade', 'cb_person_default_on_file']

    numeric_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='median')),
        ('scaler', StandardScaler())
    ])

    categorical_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='most_frequent')),
        ('onehot', OneHotEncoder(handle_unknown='ignore'))
    ])

    preprocessor = ColumnTransformer(
        transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)
        ])

    models = {
        'LogisticRegression': {
            'classifier': LogisticRegression(max_iter=500),
            'params': {
                'classifier__C': [1, 10],
                'classifier__penalty': ['l2']
            }
        },
        'RandomForestClassifier': {
            'classifier': RandomForestClassifier(random_state=42),
            'params': {
                'classifier__n_estimators': [100],
                'classifier__max_depth': [10, 20],
                'classifier__min_samples_split': [10]
            }
        },
        'GradientBoostingClassifier': {
            'classifier': GradientBoostingClassifier(random_state=42),
            'params': {
                'classifier__n_estimators': [100],
                'classifier__learning_rate': [0.1],
                'classifier__max_depth': [3]
            }
        },
        'XGBClassifier': {
            'classifier': XGBClassifier(random_state=42, use_label_encoder=False, eval_metric='logloss'),
            'params': {
                'classifier__n_estimators': [100],
                'classifier__learning_rate': [0.1],
                'classifier__max_depth': [3]
            }
        }
    }

    best_model = None
    best_auc_roc = 0
    results = []

    for model_name, model_info in models.items():
        pipeline = Pipeline([
            ('feature_engineering', FeatureEngineering()),
            ('preprocessor', preprocessor),
            ('classifier', model_info['classifier'])
        ])
        
        grid_search = GridSearchCV(pipeline, model_info['params'], cv=3, scoring='roc_auc', n_jobs=-1)
        grid_search.fit(X_train, y_train)
        
        model = grid_search.best_estimator_
        
        cv_results = cross_validate(model, X_train, y_train, cv=3, scoring=['accuracy', 'precision', 'recall', 'f1', 'roc_auc'])
        
        accuracy, precision, recall, f1, auc_roc = evaluate_model(model, X_test, y_test)
        
        results.append({
            'model_name': model_name,
            'best_parameters': grid_search.best_params_,
            'train_accuracy': np.mean(cv_results['test_accuracy']),
            'train_precision': np.mean(cv_results['test_precision']),
            'train_recall': np.mean(cv_results['test_recall']),
            'train_f1_score': np.mean(cv_results['test_f1']),
            'train_auc_roc': np.mean(cv_results['test_roc_auc']),
            'test_accuracy': accuracy,
            'test_precision': precision,
            'test_recall': recall,
            'test_f1_score': f1,
            'test_auc_roc': auc_roc
        })
        
        if auc_roc > best_auc_roc:
            best_auc_roc = auc_roc
            best_model = model

    best_result = max(results, key=lambda x: x['test_auc_roc'])
    dump(best_model, 'best_credit_risk_model.joblib')

    return TrainModelResponse(**best_result)


async def example_error_db():
    logger.error("Something went wrong")
    try:
        raise KeyError("Something went wrong")
    except Exception as e:
        logger.exception(e)
    return 0
