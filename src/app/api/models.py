from datetime import datetime
from pydantic import BaseModel, Field, validator
from pytz import timezone as tz

class LoanDataSchema(BaseModel):
    person_age: float
    person_income: float
    person_emp_length: float
    loan_amnt: float
    loan_int_rate: float
    loan_percent_income: float
    cb_person_cred_hist_length: float
    person_home_ownership: str = Field(..., min_length=3, max_length=50)
    loan_intent: str = Field(..., min_length=3, max_length=50)
    loan_grade: str = Field(..., min_length=1, max_length=2)
    cb_person_default_on_file: str = Field(..., min_length=1, max_length=1)
    created_date: datetime = datetime.now()


class LoanDataRequest(BaseModel):
    person_age: float
    person_income: float
    person_emp_length: float
    loan_amnt: float
    loan_int_rate: float
    loan_percent_income: float
    cb_person_cred_hist_length: float
    person_home_ownership: str = Field(..., min_length=3, max_length=50)
    loan_intent: str = Field(..., min_length=3, max_length=50)
    loan_grade: str = Field(..., min_length=1, max_length=2)
    cb_person_default_on_file: str = Field(..., min_length=1, max_length=1)


class PredictResponse(BaseModel):
    prediction: str
    probability_of_approval: float
    probability_of_denial: float
    

class LoanDataDB(LoanDataSchema):
    id: int
    loan_income_ratio: float
    age_cred_hist_interaction: float
    log_person_income: float

    

class TrainModelResponse(BaseModel):
    model_name: str
    best_parameters: dict
    train_accuracy: float
    train_precision: float
    train_recall: float
    train_f1_score: float
    train_auc_roc: float
    test_accuracy: float
    test_precision: float
    test_recall: float
    test_f1_score: float
    test_auc_roc: float
